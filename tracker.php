<?php

//$data = json_decode(file_get_contents('php://input'), true);

$data = file_get_contents('php://input');
$response = array();

$myfile = fopen("tracker.txt", "a") or die("Unable to open file");

if(flock($myfile, LOCK_EX | LOCK_NB)) {
    fwrite($myfile, $data);
    fwrite($myfile, "\n");
    flock($myfile, LOCK_UN);
}

fclose($myfile);

$response["status"] = "done";
header('Content-Type: application/json');
echo json_encode($response);
?>

