#!/usr/bin/env bash

source ~/.virtualenvs/phonehome/bin/activate

python phonehome/updater.py secrets.cfg save.state
python phonehome/run.py secrets.cfg save.state