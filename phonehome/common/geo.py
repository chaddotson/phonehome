from geoip import geolite2


def get_geo_info_based_on_ip(ip_address):
    match = geolite2.lookup(ip_address)

    return match.to_dict()
