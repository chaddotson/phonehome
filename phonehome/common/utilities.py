from logging.config import dictConfig
from werkzeug.utils import import_string


def configure_logging_from_dictionary(dict):
    dictConfig(dict)


def load_settings(obj_as_string):
    return import_string(obj_as_string)

