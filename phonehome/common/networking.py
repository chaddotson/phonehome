from json import dumps
from re import compile, MULTILINE
from urllib2 import Request, urlopen


def get_external_ip_address():
    url_contents = urlopen('http://www.myglobalip.com/').read()
    regex = compile("<span class=\"ip\">(.*?)</span>", MULTILINE)
    results = regex.search(url_contents)
    ip_address = results.groups()[0]
    return ip_address

def post_json(url, jsonifiable_payload):
    req = Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urlopen(req, dumps(jsonifiable_payload))