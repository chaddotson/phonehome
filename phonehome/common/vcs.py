from logging import getLogger
from os.path import exists

from git import Repo

logger = getLogger(__name__)


def fetch_code_base(source_git_repository, remote, local_directory,  branch):
    """

    :param source_git_repository:
    :param remote:
    :param local_directory:
    :param branch:
    :return:
    :throws: IndexError
    """
    logger.info("Fetching %s to %s", source_git_repository, local_directory)

    if not exists(local_directory):
        logger.info("%s doesn't exist, cloning!", local_directory)
        repo = Repo.clone_from(source_git_repository, local_directory)

    else:
        repo = Repo(local_directory)
        source_repo = repo.remotes[remote]
        source_repo.pull()

    repo.git.checkout(branch)

    return repo


