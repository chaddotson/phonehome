from argparse import ArgumentParser
from ConfigParser import RawConfigParser
from logging import getLogger
from os.path import join

from common.vcs import fetch_code_base
from common.utilities import load_settings, configure_logging_from_dictionary

logger = getLogger(__name__)



def get_args():
    parser = ArgumentParser(description='Phones Home')
    parser.add_argument('settings_file', help='ConfigParser compatible settings file.')
    parser.add_argument('persistance_file', help='File to save state to.')
    parser.add_argument('--c', '--configuration_object', help='Configuration defaults',
                        dest='configuration_object',
                        default="settings.DefaultConfiguration")

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()

    config = RawConfigParser()
    config.read(args.settings_file)

    settings = load_settings(args.configuration_object)

    configure_logging_from_dictionary(settings.LOGGING)

    #
    # state = PersistentDict(args.persistance_file)
    #
    # currentDateTime = datetime.now()
    #
    # logger.info("Last Run: %s", state.get("lastRun", currentDateTime).strftime("%c"))
    #
    # state['lastRun'] = currentDateTime
    #
    #
    #
    # state.sync()

    repo_path = join(".", "repo")

    git_url = config.get("REPO_FOR_WORK", "url")
    git_remote = config.get("REPO_FOR_WORK", "remote")
    git_branch = config.get("REPO_FOR_WORK", "branch")

    logger.info("\nRepository Info:\n - %s\n - %s\n - %s\n %s", git_url, git_remote, git_branch, repo_path)

    #repository = fetch_code_base(git_url, git_remote, repo_path, git_branch)