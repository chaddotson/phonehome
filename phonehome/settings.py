

class DefaultConfiguration(object):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '[%(levelname)s %(asctime)s %(pathname)s:%(lineno)d %(name)s]: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'NOTSET',
                'formatter': 'default'
            },
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': 'NOTSET',
                "filename": "phonehome.log",
                "maxBytes": 10485760,
                "backupCount": 0,
                "encoding": "utf8"
            }
        },
        'loggers': {
            # 'oauthlib': {
            #     'handlers': ['console', 'file'],
            #     'level': 'WARNING',
            # }
        },

        'root': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',

        }

    }
