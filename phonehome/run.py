
from argparse import ArgumentParser
from ConfigParser import RawConfigParser
from datetime import datetime
from logging import getLogger
from os.path import join
from socket import gethostname

from common.geo import get_geo_info_based_on_ip
from common.networking import get_external_ip_address, post_json
from common.utilities import load_settings, configure_logging_from_dictionary
from common.persistance import PersistentDict

logger = getLogger(__name__)

def get_args():
    parser = ArgumentParser(description='Phones Home')
    parser.add_argument('settings_file', help='ConfigParser compatible settings file.')
    parser.add_argument('persistance_file', help='File to save state to.')
    parser.add_argument('--c', '--configuration_object', help='Configuration defaults',
                        dest='configuration_object',
                        default="settings.DefaultConfiguration")
    return parser.parse_args()

def main():
    args = get_args()


    config = RawConfigParser()
    config.read(args.settings_file)

    settings = load_settings(args.configuration_object)

    configure_logging_from_dictionary(settings.LOGGING)

    #
    # state = PersistentDict(args.persistance_file)
    #
    # currentDateTime = datetime.now()
    #
    # logger.info("Last Run: %s", state.get("lastRun", currentDateTime).strftime("%c"))
    #
    # state['lastRun'] = currentDateTime
    #
    #
    #
    # state.sync()

    logger.info("Executing!")

    # print(get_external_ip_address())

    ip_info = get_geo_info_based_on_ip(get_external_ip_address())

    print(ip_info)

    data_for_post = {
        "host": gethostname(),
        "ip": ip_info["ip"],
        "location": ip_info["location"],
        "region":  iter(ip_info["subdivisions"]).next(),
        "country":  ip_info["country"],
        "timezone":  ip_info["timezone"]
    }

    post_json(settings.tracker, data_for_post)


if __name__ == '__main__':
    main()
