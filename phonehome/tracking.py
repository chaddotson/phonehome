from argparse import ArgumentParser
from ConfigParser import RawConfigParser
from datetime import datetime
from logging import getLogger
from os.path import join
from socket import gethostname

from common.geo import get_geo_info_based_on_ip
from common.networking import get_external_ip_address, post_json
from common.utilities import load_settings, configure_logging_from_dictionary
from common.persistance import PersistentDict

logger = getLogger(__name__)


def update_tracker(tracker_url):

    logger.info("Updating tracker @ %s", tracker_url)

    ip_info = get_geo_info_based_on_ip(get_external_ip_address())

    data_for_post = {
        "host": gethostname(),
        "ip": ip_info["ip"],
        "location": ip_info["location"],
        "region":  iter(ip_info["subdivisions"]).next(),
        "country":  ip_info["country"],
        "timezone":  ip_info["timezone"]
    }

    post_json(tracker_url, data_for_post)