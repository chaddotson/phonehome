from setuptools import setup

with open("requirements.txt", "r'") as f:
    install_reqs = f.readlines()

setup(name='PhoneHome',
      version='1.0',
      packages=['PhoneHome', 'PhoneHome.common'],
      install_requires=install_reqs,
      include_package_data=True,
      scripts=['bin/phonehome']
      # entry_points={
      #     'console_scripts': [
      #         'phonehome = CommanderPy.bin.TwitterDMChecker:main',
      #     ]
      # }
)
